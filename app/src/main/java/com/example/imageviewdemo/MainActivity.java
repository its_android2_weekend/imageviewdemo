package com.example.imageviewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);
        String url = "https://cdn.mos.cms.futurecdn.net/veQKQrhqeLXwXHw4Q6qM3N-320-80.jpg";

//        Glide.with(this)
//                .load("https://cdn.mos.cms.futurecdn.net/veQKQrhqeLXwXHw4Q6qM3N-320-80.jpg")
//                .into(imageView);
        Picasso.get()
                .load(url)
                //.resize(50, 50)
                //.centerCrop()
                .centerCrop()
                .into(imageView);
    }
}
